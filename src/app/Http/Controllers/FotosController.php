<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use File;

class FotosController extends Controller
{
    public function index()
    {
        $clientes = Cliente::pluck('nome','id_cliente');

        
        return view('fotos.index',['clientes' => $clientes]);
    }

    public function upload(Request $request)
    {
        $file = $request->file('image');
        $logo = $request->file('logo');

        $cod_cliente = $request->get('cod_cliente');

        $path = public_path().'/cliente_images/' . $cod_cliente;
        
        if(!File::exists($path)) {
            File::makeDirectory($path);
            File::makeDirectory($path."/logo");
        }

        $destinationPath = $path . "/";

        $logo->move($destinationPath."/logo","logo.png");

        foreach ($file as $item) {
            
            $item->move($destinationPath,$item->getClientOriginalName());
        }

        return redirect()->action('ClienteController@index');
       
    }
}
