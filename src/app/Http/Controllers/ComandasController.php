<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Comanda;
use Illuminate\Http\Request;
use Exception;

class ComandasController extends Controller
{

    /**
     * Display a listing of the comandas.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $comandas = Comanda::paginate(25);

        return view('comandas.index', compact('comandas'));
    }

    /**
     * Show the form for creating a new comanda.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('comandas.create');
    }

    /**
     * Store a new comanda in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            Comanda::create($data);

            return redirect()->route('comandas.comanda.index')
                ->with('success_message', 'Comanda was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified comanda.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $comanda = Comanda::findOrFail($id);

        return view('comandas.show', compact('comanda'));
    }

    /**
     * Show the form for editing the specified comanda.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $comanda = Comanda::findOrFail($id);
        

        return view('comandas.edit', compact('comanda'));
    }

    /**
     * Update the specified comanda in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $comanda = Comanda::findOrFail($id);
            $comanda->update($data);

            return redirect()->route('comandas.comanda.index')
                ->with('success_message', 'Comanda was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified comanda from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $comanda = Comanda::findOrFail($id);
            $comanda->delete();

            return redirect()->route('comandas.comanda.index')
                ->with('success_message', 'Comanda was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'numcomanda' => 'string|min:1|nullable', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
