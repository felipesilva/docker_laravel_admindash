<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function _construct()
    {
        
    }

    public function index(){

        $clientes = DB::table('clientes')->get();
        return view('website/home',['cliente' => $clientes]);
    }
}
