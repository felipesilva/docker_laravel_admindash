<div class="form-group {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="control-label">{{ 'Nome' }}</label>
    <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($cliente->nome) ? $cliente->nome : ''}}" >
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($cliente->email) ? $cliente->email : ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('endereco') ? 'has-error' : ''}}">
    <label for="endereco" class="control-label">{{ 'Endereco' }}</label>
    <input class="form-control" name="endereco" type="text" id="endereco" value="{{ isset($cliente->endereco) ? $cliente->endereco : ''}}" >
    {!! $errors->first('endereco', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('numero') ? 'has-error' : ''}}">
    <label for="numero" class="control-label">{{ 'Numero' }}</label>
    <input class="form-control" name="numero" type="number" id="numero" value="{{ isset($cliente->numero) ? $cliente->numero : ''}}" >
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('bairro') ? 'has-error' : ''}}">
    <label for="bairro" class="control-label">{{ 'Bairro' }}</label>
    <input class="form-control" name="bairro" type="text" id="bairro" value="{{ isset($cliente->bairro) ? $cliente->bairro : ''}}" >
    {!! $errors->first('bairro', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('cidade') ? 'has-error' : ''}}">
    <label for="cidade" class="control-label">{{ 'Cidade' }}</label>
    <input class="form-control" name="cidade" type="text" id="cidade" value="{{ isset($cliente->cidade) ? $cliente->cidade : ''}}" >
    {!! $errors->first('cidade', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('cep') ? 'has-error' : ''}}">
    <label for="cep" class="control-label">{{ 'Cep' }}</label>
    <input class="form-control" name="cep" type="text" id="cep" value="{{ isset($cliente->cep) ? $cliente->cep : ''}}" >
    {!! $errors->first('cep', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tel') ? 'has-error' : ''}}">
    <label for="tel" class="control-label">{{ 'Tel' }}</label>
    <input class="form-control" name="tel" type="text" id="tel" value="{{ isset($cliente->tel) ? $cliente->tel : ''}}" >
    {!! $errors->first('tel', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('celular') ? 'has-error' : ''}}">
    <label for="celular" class="control-label">{{ 'Celular' }}</label>
    <input class="form-control" name="celular" type="text" id="celular" value="{{ isset($cliente->celular) ? $cliente->celular : ''}}" >
    {!! $errors->first('celular', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
    <label for="facebook" class="control-label">{{ 'Facebook' }}</label>
    <input class="form-control" name="facebook" type="text" id="facebook" value="{{ isset($cliente->facebook) ? $cliente->facebook : ''}}" >
    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('instagram') ? 'has-error' : ''}}">
    <label for="instagram" class="control-label">{{ 'Instagram' }}</label>
    <input class="form-control" name="instagram" type="text" id="instagram" value="{{ isset($cliente->instagram) ? $cliente->instagram : ''}}" >
    {!! $errors->first('instagram', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
    <label for="website" class="control-label">{{ 'website' }}</label>
    <input class="form-control" name="website" type="text" id="website" value="{{ isset($cliente->website) ? $cliente->website : ''}}" >
    {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Atualizar' : 'Gravar' }}">
</div>
