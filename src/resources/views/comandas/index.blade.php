@extends('layouts.app')

@section('conteudo')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Comandas</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('comandas.comanda.create') }}" class="btn btn-success" title="Create New Comanda">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($comandas) == 0)
            <div class="panel-body text-center">
                <h4>No Comandas Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Numcomanda</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($comandas as $comanda)
                        <tr>
                            <td>{{ $comanda->numcomanda }}</td>

                            <td>

                                <form method="POST" action="{!! route('comandas.comanda.destroy', $comanda->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('comandas.comanda.show', $comanda->id ) }}" class="btn btn-info" title="Show Comanda">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('comandas.comanda.edit', $comanda->id ) }}" class="btn btn-primary" title="Edit Comanda">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Comanda" onclick="return confirm(&quot;Click Ok to delete Comanda.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $comandas->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection