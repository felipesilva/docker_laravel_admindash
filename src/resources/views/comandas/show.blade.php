@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Comanda' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('comandas.comanda.destroy', $comanda->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('comandas.comanda.index') }}" class="btn btn-primary" title="Show All Comanda">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('comandas.comanda.create') }}" class="btn btn-success" title="Create New Comanda">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('comandas.comanda.edit', $comanda->id ) }}" class="btn btn-primary" title="Edit Comanda">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Comanda" onclick="return confirm(&quot;Click Ok to delete Comanda.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Numcomanda</dt>
            <dd>{{ $comanda->numcomanda }}</dd>

        </dl>

    </div>
</div>

@endsection