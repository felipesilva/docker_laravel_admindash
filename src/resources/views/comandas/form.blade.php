
<div class="form-group {{ $errors->has('numcomanda') ? 'has-error' : '' }}">
    <label for="numcomanda" class="col-md-2 control-label">Numcomanda</label>
    <div class="col-md-10">
        <input class="form-control" name="numcomanda" type="text" id="numcomanda" value="{{ old('numcomanda', optional($comanda)->numcomanda) }}" minlength="1" placeholder="Enter numcomanda here...">
        {!! $errors->first('numcomanda', '<p class="help-block">:message</p>') !!}
    </div>
</div>

