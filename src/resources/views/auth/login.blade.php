



    <style>
        #main-content{
            margin-left: inherit;
        }
    </style>
    <div class="container">
        <div class="row card" style="margin: 0 10%; height: 320px">
            <form method="POST" action="/login" style="margin: 0 10%"> 
                @csrf
                <div class="col-md-12">
                    <center>
                        <h1>Acesso</h1>
                        <hr>
                    </center>
                </div>

                <div class="col-md-12">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="col-md-12">
                    <label for="password" class="col-md-4 col-form-label text-md-right">Senha</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="col-md-12">
                    <hr>
                </div>

                

                <div class="col-md-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            Manter Conectado
                        </label>
                        <br>
                        @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Esqueceu a Senha?
                        </a>
                    @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Login') }}
                    </button>

                    
                </div>
            </form>
        </div>
    </div>
        

