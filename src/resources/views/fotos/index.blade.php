@extends('layouts.app')

@section('conteudo')
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>Fotos</h1>
        </div>
        <div class="col-md-12 ">
        <?php
         echo Form::open(array('url' => '/upload','files'=>'true'));
         ?>
        <div class="form-group">
            <h4>Selecione o cliente</h4>
            <select class="form-input" name="cod_cliente">
                @foreach($clientes as $key => $value)
                    <option  value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <?php
        echo 'Selecione o logo';
        echo Form::file('logo');

        echo 'Selecione as fotos:';
        echo Form::file('image[]',['multiple' => 'multiple']);
        echo Form::submit('Enviar');
        echo Form::close();
      ?>
            

        </div>
    </div>
@endsection