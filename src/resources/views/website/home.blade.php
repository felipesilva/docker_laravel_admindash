
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Radar da Esquadrias &mdash; Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700|Work+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="website/fonts/icomoon/style.css">

    <link rel="stylesheet" href="website/css/bootstrap.min.css">
    <link rel="stylesheet" href="website/css/magnific-popup.css">
    <link rel="stylesheet" href="website/css/jquery-ui.css">
    <link rel="stylesheet" href="website/css/owl.carousel.min.css">
    <link rel="stylesheet" href="website/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="website/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="website/css/animate.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">
    
    
    
    <link rel="stylesheet" href="website/fonts/flaticon/font/flaticon.css">
  
    <link rel="stylesheet" href="website/css/aos.css">

    <link rel="stylesheet" href="website/css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    
    <div class="site-navbar-wrap js-site-navbar bg-white">
      
      <div class="container">
        <div class="site-navbar bg-light">
          <div class="py-1">
            <div class="row align-items-center">
              <div class="col-2">
                <h2 class="mb-0 site-logo"><a href="/"><img src="/website/images/logo.jpeg" width="150px"></a></h2>
              </div>
              <div class="col-10">
                <nav class="site-navigation text-right" role="navigation">
                  <div class="container">
                    <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

                    <ul class="site-menu js-clone-nav d-none d-lg-block">
                      <li><a href="/todos">Clientes</a></li>
                      
                      <li><a href="/contato">Contato</a></li>
                      <!--<li><a href="new-post.html"><span class="bg-primary text-white py-3 px-4 rounded btn-home"><span class="icon-plus mr-3"></span>Anuncie</span></a></li>-->
                    </ul>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  
    <div style="height: 113px;"></div>

    <div class="site-blocks-cover overlay" style="background-image: url('/website/images/fundo.png');" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-12 text-center" data-aos="fade" style="margin-top: 60px">
            <h2 class="titulo-home">
            As melhores empresas de esquadrias em um só lugar
            </h2>
          </div>
          <div class="col-12" data-aos="fade" style="margin-top:-200px">
            <h1 style="text-shadow: 2px 2px black">Busca</h1>
            
            <div class="row mb-3">
              <div class="col-md-9">
                <div class="row">
                  
                  <div class="col-md-12 mb-6 mb-md-0">
                    <div class="input-wrap">
                      <span class="icon icon-room"></span>
                    <input type="text" class="form-control form-control-block search-input  border-0 px-4" id="busca" placeholder="Digite a sua CIDADE..." onFocus="geolocate()">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <button class="btn btn-search btn-primary btn-block btn-home" id="btnBusca">Buscar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    

    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mx-auto text-center mb-5 section-heading">
            <h2 class="mb-5">Clientes</h2>
          </div>
        </div>
        <div id='div_clientes' class="row">
          @foreach($cliente as $item)
            <div class="col-sm-6 col-md-4 col-lg-3 mb-3 card text-center" data-aos="fade-up" data-aos-delay="100">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12"><img class="img-thumbnail" src="/cliente_images/{{$item->id_cliente}}/logo/logo.png" alt="cliente padrao"></div>
                  <div class="col-md-12"><h3>{{$item->nome}}</h3></div>
                  <div class="col-md-12"><h6>{{$item->email}}</h6></div>
                  <div class="col-md-6">
                    <center>
                      <a href="{{$item->facebook}}" target="_blank" rel="noopener noreferrer">
                        <i class="icone fa fa-facebook 7x"></i>
                      </a>
                    </center>
                  </div>
                  <div class="col-md-6">
                    <center>
                      <a href="{{$item->instagram}}" target="_blank" rel="noopener noreferrer">
                        <i class="icone fa fa-instagram 7x"></i>
                      </a>
                    </center>
                  </div>
                  <div class="col-md-12">
                    <a href="/cliente/{{$item->id_cliente}}">Mais Info</a>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
          
        </div>

      </div>
    </div>




    
    <footer class="site-footer">
      <div class="container">
        

        
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-4 item-rodape">
            <h4 class="text-white">Telefone:</h4>
            <p>(11) 97251-5111</p>
          </div>

          <div class="col-md-4 item-rodape">
            <a href="https://www.instagram.com/radaresquadrias/?hl=pt-br" target="_blank">
              <h4 class="text-white">Instagram:</h4>
              <i class="fa fa-instagram"></i>
            </a>
          </div>

          <div class="col-md-4 item-rodape">
          <a href="https://www.facebook.com/Radar-esquadrias-100490784730433" target="_blank">
              <h4 class="text-white">Facebook:</h4>
              <i class="fa fa-facebook"></i>
            </a>
          </div>

          <div class="col-md-12 text-center item-rodape">
            <a href="mailto:contato@radardasesquadrias.com.br">
              <h4 class="text-white">E-mail:</h4>
              <p>contato@radardasesquadrias.com.br</p>
            </a>
          </div>
          
        </div>
      </div>
    </footer>
  </div>

  <script src="website/js/jquery-3.3.1.min.js"></script>
  <script src="website/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="website/js/jquery-ui.js"></script>
  <script src="website/js/popper.min.js"></script>
  <script src="website/js/bootstrap.min.js"></script>
  <script src="website/js/owl.carousel.min.js"></script>
  <script src="website/js/jquery.stellar.min.js"></script>
  <script src="website/js/jquery.countdown.min.js"></script>
  <script src="website/js/jquery.magnific-popup.min.js"></script>
  <script src="website/js/bootstrap-datepicker.min.js"></script>
  <script src="website/js/aos.js"></script>

  
  <script src="website/js/mediaelement-and-player.min.js"></script>

  <script src="website/js/main.js"></script>
    

  <script>
      document.addEventListener('DOMContentLoaded', function() {
                var mediaElements = document.querySelectorAll('video, audio'), total = mediaElements.length;

                for (var i = 0; i < total; i++) {
                    new MediaElementPlayer(mediaElements[i], {
                        pluginPath: 'https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/',
                        shimScriptAccess: 'always',
                        success: function () {
                            var target = document.body.querySelectorAll('.player'), targetTotal = target.length;
                            for (var j = 0; j < targetTotal; j++) {
                                target[j].style.visibility = 'visible';
                            }
                  }
                });
                }
            });
    </script>

    <script>
    $('#btnBusca').click(function(){

      var busca = $('#busca').val();

      //debugger;

      $.get('/pesquisar?busca=' + busca,function(data){
        var retorno =  JSON.parse(data);
        //var cont = retorno.length;

        var html = "";

        if(retorno.length > 0){
          for (let index = 0; index < retorno.length; index++) {
            const element = retorno[index];

            console.log(element);

            html = html + `<div class="col-sm-6 col-md-4 col-lg-3 mb-3 card text-center" data-aos="fade-up" data-aos-delay="100">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-12"><img class="img-thumbnail" src="/cliente_images/`+ element.id_cliente +`/logo/logo.png" alt="cliente padrao"></div>
                    <div class="col-md-12"><h3>`+ element.nome +`</h3></div>
                    <div class="col-md-12"><h6>`+ element.email +`</h6></div>
                    <div class="col-md-6">
                      <center>
                        <a href="`+ element.facebook +`" target="_blank" rel="noopener noreferrer">
                          <i class="icone fa fa-facebook 7x"></i>
                        </a>
                      </center>
                    </div>
                    <div class="col-md-6">
                      <center>
                        <a href="`+ element.instagram +`" target="_blank" rel="noopener noreferrer">
                        <i class="icone fa fa-instagram 7x"></i>
                        </a>
                      </center>
                    </div>
                    <div class="col-md-12">
                      <a href="/cliente/`+ element.id_cliente +`">Mais Info</a>
                    </div>
                  </div>
                </div>
              </div>`;

            
            
           
          }

          $('#div_clientes').html(html);
        }
        else{
          $('#div_clientes').html("<center><h3>Nenhum cliente encontrado!</h3></center>");
        }

          // Scroll
          $('html,body').animate({
                scrollTop: $("#div_clientes").offset().top
          }, 'slow');

      });

    });
    
    </script>


    <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&libraries=places&callback=initAutocomplete"
        async defer></script>

  </body>
</html>